#include "common.h"
namespace common
{
unsigned int bArrToUInt(QByteArray* arr)
{
    unsigned int result = 0;
    for(int i = 0; i < 4 && i < arr->size(); i++)
    {
        result = result << 8; // if 0 then = 0;
        unsigned int interstage = (unsigned int)(arr->at(i));
        interstage &= 0x00ff;
        result |= interstage;
    }
    return result;
};

QByteArray uintToBArr(unsigned int number)
{
    QByteArray arr;
    for(int i = 3; i >= 0; i--)
    {
        arr.append((char)((number >> (i * 8)) & 0x00ff));
    }
    return arr;
};
}//common
