#ifndef COMMON_H
#define COMMON_H

#include <QByteArray>
namespace common
{

unsigned int bArrToUInt(QByteArray* arr);
QByteArray uintToBArr(unsigned int number);

};//common
#endif // COMMON_H
