#include "roomgraphicsobject.h"
#include "data/fly.h"
#include "graphics/flygraphicsobject.h"
#include "data/roomcell.h"
#include "common/common.h"
#include <QGraphicsSceneDragDropEvent>
#include <QMimeData>
#include <QPainter>
#include <QDebug>
#include <QGraphicsScene>
#include <cmath>

RoomGraphicsObject::RoomGraphicsObject(Room *room, double cellSize)
    : QGraphicsObject(nullptr),
      m_room(room),
      m_cellSize(cellSize)
{
    setZValue(-1000);// just for view it always at bottom level
    setAcceptDrops(true);
}

QRectF RoomGraphicsObject::boundingRect() const
{
    if(!m_room) return QRectF();
    return QRectF(QPointF(0, 0), QSizeF(m_cellSize * m_room->size(), m_cellSize * m_room->size()));
}

void RoomGraphicsObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    auto roomSize = m_room->size();
    for(double i = 0; i <= roomSize; i++)
    {
        for(double j = 0; j <= roomSize; j++)
        {
            painter->drawLine(QPointF(i * m_cellSize, 0), QPointF(i * m_cellSize, roomSize * m_cellSize));
            painter->drawLine(QPointF(0, j * m_cellSize), QPointF(roomSize * m_cellSize, j * m_cellSize));
        }
    }
}

void RoomGraphicsObject::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    if (m_room && event->mimeData()->hasFormat("flyData"))
            event->acceptProposedAction();
}

void RoomGraphicsObject::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    auto data = event->mimeData()->data("flyData");
    unsigned int stupidity = common::bArrToUInt(&data);
    event->accept();
    if(stupidity == 0)// no rooms for genious
        return;
    Fly *fly = new Fly(m_room->getFreeId(),
                       stupidity, m_room,
                       getCellCoord(event->scenePos()),
                       m_room->size(), m_room);
    if(!m_room->addNewFly(fly))
    {
        delete fly;
    }
    else
    {
        FlyGraphicsObject *flyRepr = new FlyGraphicsObject(fly, this);
        scene()->addItem(flyRepr);
    }
}

QPointF RoomGraphicsObject::getCellCenter(QPoint cellCoord)
{
    return getCellTopLeft(cellCoord) + QPointF(m_cellSize / 2.0, m_cellSize / 2.0);
}

QPointF RoomGraphicsObject::getCellTopLeft(QPoint cellCoord)
{
    auto roomSize = m_room->size();
    if(cellCoord.x() < 0 || cellCoord.x() > roomSize ||
       cellCoord.y() < 0 || cellCoord.y() > roomSize )
    {
        return QPointF(-1, -1);
    }
    return pos() + QPointF(cellCoord) * m_cellSize;
}

QPoint RoomGraphicsObject::getCellCoord(QPointF scenePos)
{
    auto roomSize = m_room->size();
    scenePos -= pos();
    if(scenePos.x() < 0 || scenePos.x() > m_cellSize * roomSize ||
       scenePos.y() < 0 || scenePos.y() > m_cellSize * roomSize )
    {
        return QPoint(-1, -1);
    }
    else
    {
        return QPoint(scenePos.x() / m_cellSize, scenePos.y() / m_cellSize); // floor values
    }
}

QPointF RoomGraphicsObject::getCellInnerPosition(QPoint cellCoord, unsigned int flyId)
{
    auto cell = m_room->getCell(cellCoord);
    if(!cell)
    {
        return QPointF(-1, -1); // yap, it's invalid magick point. sorry!
    }
    int flyNumber = cell->flyPos(flyId);//it will be equal to -1 only when something goes wrong
    if(flyNumber == -1) return QPointF(-1, -1);

    int size = ceil(sqrt(cell->capacity()));
    QPoint coord = QPoint(flyNumber / size, flyNumber % size);
    QPointF pos = getCellTopLeft(cellCoord) + QPointF(coord.x() * (m_cellSize / size),
                                                      coord.y() * (m_cellSize / size));
    pos += QPointF(m_cellSize / size / 2.0, m_cellSize / size / 2.0);
    return pos;
}

void RoomGraphicsObject::onStarted()
{
    setAcceptDrops(false);
}

void RoomGraphicsObject::onStopped()
{
    setAcceptDrops(true);
}
