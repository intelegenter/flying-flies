#ifndef FLYGRAPHICSOBJECT_H
#define FLYGRAPHICSOBJECT_H

#include "data/fly.h"
#include "graphics/roomgraphicsobject.h"
#include <QGraphicsObject>
#include <QGraphicsTextItem>

class FlyGraphicsObject : public QGraphicsObject
{
    Q_OBJECT
public:
    explicit FlyGraphicsObject(Fly *fly, RoomGraphicsObject *room);
    ~FlyGraphicsObject();
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

signals:

public slots:
private slots:
    void onFlyAcceptDesigion(QPoint newDesigion);
    void onFlyPositionChanged(QPoint newPos);
    void onFlyDead();
    void onFlySays(QString phrase);
    void onPhraseReadyToDelete();

protected:
    Fly *m_fly = nullptr;
    QPixmap *m_pixmap = nullptr;
    RoomGraphicsObject *m_room = nullptr;
    QGraphicsTextItem *m_phrase = nullptr;
    QTimer *m_phraseResetTimer = nullptr;
};

#endif // FLYGRAPHICSOBJECT_H
