#include "flygraphicsobject.h"
#include <QPainter>
#include <QTimer>
#include <QApplication>

FlyGraphicsObject::FlyGraphicsObject(Fly *fly, RoomGraphicsObject *room)
    : QGraphicsObject(nullptr),
      m_fly(fly)
{
    setParent(m_fly);
    m_pixmap = new QPixmap;
    *m_pixmap = QPixmap(":/sources/resources/images/fly.png");
    m_room = room;
    if(m_fly && m_room)
    {
        setPos(m_room->getCellInnerPosition(m_fly->pos(), m_fly->id()));
        connect(fly, &Fly::newDesigionAccepted, this, &FlyGraphicsObject::onFlyAcceptDesigion);
        connect(fly, &Fly::positionChanged, this, &FlyGraphicsObject::onFlyPositionChanged);
        connect(fly, &Fly::iAmDied, this, &FlyGraphicsObject::onFlyDead);
        connect(fly, &Fly::iSay, this, &FlyGraphicsObject::onFlySays);
    }
    else
    {
        setPos(-1, -1);
    }

    m_phraseResetTimer = new QTimer(this);
    m_phraseResetTimer->setSingleShot(true);
    connect(m_phraseResetTimer, &QTimer::timeout, this, &FlyGraphicsObject::onPhraseReadyToDelete);
    m_phrase = new QGraphicsTextItem(this);
    QFont font = m_phrase->font();
    font.setPixelSize(50); // so iteration magick. 50 - is better value.
    m_phrase->setFont(font);
}

FlyGraphicsObject::~FlyGraphicsObject()
{
    if(m_pixmap)
    {
        delete m_pixmap;
        m_pixmap = nullptr;
    }
    if(m_phrase)
    {
        delete m_phrase;
        m_phrase = nullptr;
    }
}

QRectF FlyGraphicsObject::boundingRect() const
{
    return QRectF(QPointF(-m_pixmap->size().width() / 2,
                          -m_pixmap->size().height() / 2),
                  m_pixmap->size());
}

void FlyGraphicsObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawPixmap(boundingRect(), *m_pixmap, m_pixmap->rect());
    if(m_fly && !m_fly->isDead());
    {
        QFont font;
        font.setPixelSize(100);
        painter->setFont(font);
        painter->drawText(boundingRect().topLeft() + QPointF(0, 100), QString::number(m_fly->stupidity() / 1000.0, 'g', 5));
        painter->setPen(Qt::red);
        QString flyId = QString::number(m_fly->id());
        QFontMetrics fm(font);
        QPointF shift(-fm.width(flyId) / 2, fm.height() / 2);
        painter->drawText(boundingRect().center() + shift, flyId);
    }
}

void FlyGraphicsObject::onFlyAcceptDesigion(QPoint newDesigion)
{
    QPointF newPos = m_room->getCellCenter(newDesigion);
    QLineF eyeLine(pos(), newPos);
    setRotation(-eyeLine.angle() + 90); // cause in pixmap fly is turned to 90
}

void FlyGraphicsObject::onFlyPositionChanged(QPoint newPos)
{
    setPos(m_room->getCellInnerPosition(newPos, m_fly->id()));
}

void FlyGraphicsObject::onFlyDead()
{
    *m_pixmap = QPixmap(":/sources/resources/images/dead_fly.png");
    update(boundingRect());
}

void FlyGraphicsObject::onFlySays(QString phrase)
{
    QFontMetrics fm(m_phrase->font());
    m_phrase->setPlainText(phrase);
    m_phrase->setPos(-fm.width(phrase) / 2, boundingRect().bottom() + fm.height());
    m_phraseResetTimer->start(1000);
}

void FlyGraphicsObject::onPhraseReadyToDelete()
{
    m_phrase->setPlainText("");
}
