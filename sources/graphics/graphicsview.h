#ifndef GRAPHICSVIEW_H
#define GRAPHICSVIEW_H

#include <QGraphicsView>

class GraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit GraphicsView(QWidget *parent = nullptr);

    void wheelEvent(QWheelEvent *event) override;
signals:

public slots:
};

#endif // GRAPHICSVIEW_H
