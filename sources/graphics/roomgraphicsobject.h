#ifndef ROOMGRAPHICSOBJECT_H
#define ROOMGRAPHICSOBJECT_H

#include "data/room.h"
#include <QGraphicsObject>

class RoomGraphicsObject : public QGraphicsObject
{
    Q_OBJECT
public:
    RoomGraphicsObject(Room *room, double cellSize);
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    void dragEnterEvent(QGraphicsSceneDragDropEvent  *event) override;
    void dropEvent(QGraphicsSceneDragDropEvent * event) override;
    QPointF getCellCenter(QPoint cellCoord);
    QPointF getCellTopLeft(QPoint cellCoord);
    QPoint getCellCoord(QPointF scenePos);
    QPointF getCellInnerPosition(QPoint cellCoord, unsigned int flyId);

public slots:
    void onStarted();
    void onStopped();

protected:

    Room *m_room;
    double m_cellSize;
};

#endif // ROOMGRAPHICSOBJECT_H
