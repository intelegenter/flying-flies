#include "graphicsview.h"
#include <QDragEnterEvent>
#include <QMimeData>
#include <QDebug>
#include <QGraphicsSceneDragDropEvent>
#include <QWheelEvent>

GraphicsView::GraphicsView(QWidget *parent) : QGraphicsView(parent)
{

}

void GraphicsView::wheelEvent(QWheelEvent *event)
{
    if(event->delta() < 0)
    {
        scale(0.9,0.9);
    }
    else
    {
        scale(1.1, 1.1);
    }
    update();
}

