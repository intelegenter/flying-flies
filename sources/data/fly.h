#ifndef FLY_H
#define FLY_H

#include <QObject>
#include <QPoint>

class Room;
class QTimer;
class QThread;
class Fly : public QObject
{
    Q_OBJECT
public:
    Fly(unsigned int id, unsigned int stupidity, Room *room, QPoint pos, unsigned int lifeTimeCoeff, QObject *parent = nullptr);
    ~Fly();
    bool isDead();
    void bringToLife();
    void stop();
    QPoint pos();
    unsigned int stupidity();
    void life(); // main thread function
    void onTimeToGo();
    int run();
    unsigned int lifeTime();

    unsigned long id() const;

    bool isInterrupted() const;
    void kill();

signals:
    void newDesigionAccepted(QPoint newDesigion);
    void positionChanged(QPoint newPos);
    void iAmDied();
    void iAmAlive();
    void iSay(QString str);

private slots:                                                                                                                                     //42
protected:
    void say(QString str, bool broadcast = true);
    void onThreadFinished();

    // time in ms;
    Room *m_room = nullptr;
    uint64_t m_lifetime = 0;
    unsigned int m_stupidity = 0;
    unsigned int m_timeToDie = 0;
    uint64_t m_lastDesigionTime = 0;
    int m_run = 0;
    unsigned long m_id = -1; // magic number is too bad but... it will not be used, just indicator.
    QPoint m_pos;
    QPoint m_desigion;
    bool m_amIDead = true;
    bool m_desigionAccepted = false;
    bool m_isIdle = false;
    bool m_isInterrupted = false;
    QThread* m_thread = nullptr;
};

#endif // FLY_H
