#include "room.h"
#include "roomcell.h"
#include "fly.h"
#include <QDebug>

QMap<int, QPoint> Room::scanDirection =
{
    {0, {-1, -1}},
    {1, {0, -1}},
    {2, {1, -1}},
    {3, {1, 0}},
    {4, {1, 1}},
    {5, {0, 1}},
    {6, {-1, 1}},
    {7, {-1, 0}}
};

Room::Room(unsigned int cellCapacity, unsigned int size, QObject *parent)
    : QObject(parent),
      m_size(size)
{
    m_cells.resize(m_size * m_size);
    for(int i = 0; i < m_size; i++)
    {
        for(int j = 0; j < m_size; j++)
        {
            m_cells[j * m_size + i] = new RoomCell(QPoint(i, j), cellCapacity);
        }
    }
}

Room::~Room()
{
    for(int i = 0; i < m_cells.size(); i++)
    {
        delete m_cells[i];
        m_cells[i] = nullptr;
    }
    m_cells.clear();
}

RoomCell *Room::getCell(QPoint coord)
{
    return getCell(coord.x(), coord.y());
}

RoomCell *Room::getCell(int x, int y)
{
    if(x < 0 || x >= m_size ||
       y < 0 || y >= m_size )
        return nullptr;
    return m_cells[y * m_size + x];
}

QList<RoomCell *> Room::getNeighbourCells(QPoint coord)
{
    return getNeighbourCells(coord.x(), coord.y());
}

QList<RoomCell *> Room::getNeighbourCells(int x, int y)
{
    QList<RoomCell*> res;

    int ind = rand() % scanDirection.size();
    for(int i = 0; i < scanDirection.size(); i++)
    {
        if(ind >= scanDirection.size())
        {
            ind = 0;
        }
        QPoint coord = QPoint(x + scanDirection[ind].x(),
                               y + scanDirection[ind].y());
        auto cell = getCell(coord);
        if(cell)
        {
            res.append(cell);
        }
    }
    return res;
}

bool Room::addNewFly(Fly *fly)
{
    auto cell = getCell(fly->pos());
    if(!cell) return false;
    if(cell->fliesCount() > cell->capacity())
    {
        return false;
    }
    else
    {
        if(!cell->addFly(fly))
        {
            return false;
        }
        else
        {
            m_fliesPool.push_back(fly);
            connect(fly, &Fly::iAmDied, this, &Room::onFlyDied);
            return true;
        }
    }
}

int Room::size()
{
    return m_size;
}

const QVector<Fly*>* Room::graveyard() const
{
    return &m_graveyard;
}

const QVector<Fly *> *Room::aliveFlies() const
{
    return &m_fliesPool;
}

void Room::start()
{
    for(auto fly : m_fliesPool)
    {
        fly->bringToLife();
    }
}

void Room::stop()
{
    for(auto fly : m_fliesPool)
    {
        fly->stop();
    }
}

unsigned int Room::getFreeId()
{
    return m_idCounter++;
}

void Room::onFlyDied()
{
    Fly* fly = qobject_cast<Fly*>(sender());
    if(fly)
    {
        m_fliesPool.removeOne(fly);
        m_graveyard.push_back(fly);
    }
}

void Room::printFlyStats(Fly* fly)
{
    if(!fly) return;
    qDebug() << "fly " << fly->id() << " flied " << fly->run();
}

void Room::clearFlies()
{
    for(auto fly : m_fliesPool)
    {
        delete fly;
    }
    for(auto fly : m_graveyard)
    {
        delete fly;
    }
    m_graveyard.clear();
    m_fliesPool.clear();
}
