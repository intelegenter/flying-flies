#ifndef ROOM_H
#define ROOM_H

#include <QObject>
#include <QVector>

class Fly;
class RoomCell;
class Room : public QObject
{
    Q_OBJECT
public:
    Room(unsigned int cellCapacity, unsigned int size, QObject *parent = nullptr);
    ~Room();
    RoomCell* getCell(QPoint coord);
    RoomCell* getCell(int x, int y);
    QList<RoomCell*> getNeighbourCells(QPoint coord);
    QList<RoomCell*> getNeighbourCells(int x, int y);
    bool addNewFly(Fly* fly);
    int size();
    const QVector<Fly*>* graveyard() const;
    const QVector<Fly*>* aliveFlies() const;
    void start();
    void stop();
    unsigned int getFreeId();

signals:

public slots:
    void onFlyDied();
    void printFlyStats(Fly* fly);
    void clearFlies();

protected:
    unsigned int m_idCounter = 1; // currentrly by this, maybe will be changed (cause 0 - is invalid fly id)
    QVector<RoomCell*> m_cells;
    unsigned int m_size;
    QVector<Fly*> m_graveyard; //graveyard
    QVector<Fly*> m_fliesPool;

    static QMap<int, QPoint> scanDirection;
};

#endif // ROOM_H
