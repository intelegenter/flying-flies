#ifndef ROOMCELL_H
#define ROOMCELL_H

#include <QVector>
#include <QPoint>

class QMutex;
class Fly;
class RoomCell
{
public:
    RoomCell(QPoint coordinates, unsigned int capacity);
    ~RoomCell();
    bool addFly(Fly* Fly);
    void removeFly(Fly* Fly);
    unsigned int fliesCount();
    unsigned int capacity();
    QPoint coordinates();
    int flyPos(unsigned int id);


protected:
    int getFreeIndex();

    QMutex *m_fliesCountMutex;
    QMutex *m_fliesVectorMutex;
    QVector<unsigned int> m_flies;
    unsigned int m_fliesCount = 0;
    const unsigned int m_capacity;
    QPoint m_coord;
};

#endif // ROOMCELL_H
