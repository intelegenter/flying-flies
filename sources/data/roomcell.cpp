#include "roomcell.h"
#include <QMutex>
#include "fly.h"

RoomCell::RoomCell(QPoint coordinates, unsigned int capacity)
    :m_capacity(capacity),
     m_coord(coordinates)
{
    m_flies.resize(m_capacity);
    m_flies.fill(0); // zero as invalid thread id
    m_fliesCountMutex = new QMutex;
    m_fliesVectorMutex = new QMutex;
}

RoomCell::~RoomCell()
{
    delete m_fliesCountMutex;
    delete m_fliesVectorMutex;
}

bool RoomCell::addFly(Fly *fly)
{
    m_fliesVectorMutex->lock();
    m_fliesCountMutex->lock();
    bool res = true;
    if(m_fliesCount >= m_capacity)
    {
        res = false;
    }
    else
    {
        int ind = getFreeIndex();
        if(ind == -1) // no free index
        {
            assert(false);
            res = false;
        }
        else
        {
            m_flies[ind] = fly->id();
            m_fliesCount++;
        }
    }
    m_fliesCountMutex->unlock();
    m_fliesVectorMutex->unlock();
    return res;
}

void RoomCell::removeFly(Fly *fly)
{
    m_fliesVectorMutex->lock();
    m_fliesCountMutex->lock();

    for(int i = 0; i < m_flies.size(); i++)
    {
        if(m_flies[i] == fly->id())
        {
            m_flies[i] = 0; // set invalid fly id
            m_fliesCount--;
            break;
        }
    }

    m_fliesCountMutex->unlock();
    m_fliesVectorMutex->unlock();
}

unsigned int RoomCell::fliesCount()
{
    m_fliesCountMutex->lock();
    unsigned int res = m_fliesCount;
    m_fliesCountMutex->unlock();
    return res;
}

unsigned int RoomCell::capacity()
{
    return m_capacity;
}

QPoint RoomCell::coordinates()
{
    return m_coord;
}

int RoomCell::flyPos(unsigned int id)
{
    for(int i = 0; i < m_flies.size(); i++)
    {
        if(m_flies[i] == id)
            return i;
    }
    return -1;
}

int RoomCell::getFreeIndex()
{
    for(int i = 0; i < m_flies.size(); i++)
    {
        if(m_flies[i] == 0)
            return i;
    }
    return -1;
}
