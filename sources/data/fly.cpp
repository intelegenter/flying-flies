#include "fly.h"
#include <QTimer>
#include <QThread>
#include <QDebug>
#include <QTime>
#include "roomcell.h"
#include "room.h"


Fly::Fly(unsigned int id, unsigned int stupidity, Room *room, QPoint pos, unsigned int lifeTimeCoeff, QObject *parent)
    : QObject(parent),
    m_room(room),
    m_stupidity(stupidity),
    m_id(id),
    m_pos(pos)
{
    if(!room)
    {
        qDebug() << "fly can't be invoked, because no more rooms in hell";
        assert(false);
        return;
    }
    m_timeToDie = lifeTimeCoeff * m_stupidity;
    say("i will die after " + QString::number(m_timeToDie) + " ms", false);
}

Fly::~Fly()
{
    if(m_thread)
    {
        if(m_thread->isRunning())
        {
            m_isInterrupted = true;
            m_thread->wait();
        }
        delete m_thread;
    }
}

bool Fly::isDead()
{
    return m_amIDead;
}

void Fly::bringToLife()
{
    m_amIDead = false;
    emit iAmAlive();
    if(!m_room || m_pos.x() < 0 ||
       m_pos.x() >= m_room->size() ||
       m_pos.y() < 0 ||
       m_pos.y() >= m_room->size())
    {
        say("Where am i?... it seems like my time has not come yet...", false);
        m_amIDead = true;
        emit iAmDied();
        return;
    }
    m_thread = QThread::create([this](){
        life();
    });
    connect(m_thread, &QThread::finished, this, &Fly::onThreadFinished);
    m_thread->start();
}

void Fly::stop()
{
    m_isInterrupted = true;
    if(m_thread && m_thread->isRunning())
    {
        m_thread->wait();
    }
}

QPoint Fly::pos()
{
    return m_pos;
}

unsigned int Fly::stupidity()
{
    return m_stupidity;
}

void Fly::onTimeToGo()
{
    say("Let see!", false);
    m_isIdle = false;
    m_lifetime += m_stupidity;// simple simplifying. No matter real elapsed time, cause difference will be small
    if(m_desigionAccepted)
    {
        auto cell = m_room->getCell(m_desigion);
        if(cell->addFly(this))
        {
            auto oldCell = m_room->getCell(m_pos);
            oldCell->removeFly(this);
            m_pos = cell->coordinates();
            m_run++;
            say("Wheee!!!");
            emit positionChanged(m_pos);
        }
        else
        {
            say("Damn it!!!");
        }
        m_desigionAccepted = false;
    }
    else say("Nothing there :(");
    if(m_lifetime > m_timeToDie) //time to die, fly!
    {
        m_amIDead = true; //cruelly kill a fly T_T
    }
}

int Fly::run()
{
    return m_run;
}

unsigned int Fly::lifeTime()
{
    return m_lifetime;
}

void Fly::say(QString str, bool broadcast)
{
    if(broadcast)
    {
        emit iSay(str);
    }
    str = ("Number " +
                QString::number(m_id) +
                " reported from " +
                QString::number(m_pos.x()) + "; " +
                QString::number(m_pos.y()) + ": " +
                str);
    qDebug() << str;
}

void Fly::onThreadFinished()
{
    delete m_thread;
    m_thread = nullptr;
}

bool Fly::isInterrupted() const
{
    return m_isInterrupted;
}

void Fly::kill()
{
    m_amIDead = true;
    emit iAmDied();
}

unsigned long Fly::id() const
{
    return m_id;
}

void Fly::life()
{
    say("What a beautiful new world!!!", false);
    srand(m_id);
    while(!m_amIDead)
    {
        if(m_isInterrupted) return;
        if(!m_desigionAccepted && !m_isIdle)
        {
            say("where i can go?", false);
            QList<RoomCell*> neigbours = m_room->getNeighbourCells(m_pos);
            for(auto cell : neigbours)
            {
                if(cell->fliesCount() < cell->capacity())
                {
                    m_desigion = cell->coordinates();
                    say("i can go to " + QString::number(m_desigion.x()) + "; " + QString::number(m_desigion.y()), false);
                    m_desigionAccepted = true;
                    emit newDesigionAccepted(m_desigion);
                    break;
                }
            }
            if(!m_desigionAccepted)
            {
                say("I am stuck!!!");
            }
            m_isIdle = true;
            m_lastDesigionTime = QTime::currentTime().msecsSinceStartOfDay();
        }
        else
        {
            if(m_lastDesigionTime + m_stupidity < QTime::currentTime().msecsSinceStartOfDay())
                onTimeToGo();
        }
        QThread::currentThread()->yieldCurrentThread();
    }
    say("For what?!... khhh...khhh...");
    emit iAmDied();
}
