#include "gui/mainwindow.h"
#include "gui/inputdialog.h"

#include <QApplication>
#include <QPoint>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    InputDialog *dial = new InputDialog();
    dial->exec();
    if(dial->result() == InputDialog::Accepted)
    {
        MainWindow w(dial->roomSize(), dial->flyCapacity());
        w.show();
        w.fitView();
        delete dial;
        return a.exec();
    }

    return -1;
}
