#include "inputdialog.h"
#include "ui_inputdialog.h"

InputDialog::InputDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::InputDialog)
{
    ui->setupUi(this);
}

InputDialog::~InputDialog()
{
    delete ui;
}

int InputDialog::roomSize() const
{
    return m_roomSize;
}

int InputDialog::flyCapacity() const
{
    return m_flyCapacity;
}

void InputDialog::on_spinRoomSize_valueChanged(int arg1)
{
    m_roomSize = arg1;
}

void InputDialog::on_spinFlyCapacity_valueChanged(int arg1)
{
    m_flyCapacity = arg1;
}
