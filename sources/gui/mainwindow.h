#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QMap>
#include <data/fly.h>
#include <data/room.h>
#include "graphics/roomgraphicsobject.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(int roomSize, int flyCap, QWidget *parent = nullptr);
    ~MainWindow();
    void fitView();

private slots:
    void on_spinBox_valueChanged(int arg1);

    void on_playPauseBtn_toggled(bool checked);

    void on_clearButton_clicked();

private:
    QString formFlyStats();
    Ui::MainWindow *ui = nullptr;
    Room *m_room = nullptr;
    RoomGraphicsObject *m_roomRepr = nullptr;
    int m_roomSize = 0;
    int m_flyCap = 0;
    const double m_cellPixelSize = 500;

};
#endif // MAINWINDOW_H
