#ifndef INPUTDIALOG_H
#define INPUTDIALOG_H

#include <QDialog>

namespace Ui {
class InputDialog;
}

class InputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InputDialog(QWidget *parent = nullptr);
    ~InputDialog();

    int roomSize() const;

    int flyCapacity() const;

private slots:
    void on_spinRoomSize_valueChanged(int arg1);

    void on_spinFlyCapacity_valueChanged(int arg1);

private:
    Ui::InputDialog *ui;
    int m_roomSize = 0;
    int m_flyCapacity = 0;
};

#endif // INPUTDIALOG_H
