#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "graphics/graphicsview.h"
#include <QDebug>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QTimer>
#include <QGraphicsScene>

MainWindow::MainWindow(int roomSize, int flyCap, QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_roomSize(roomSize),
    m_flyCap(flyCap)
{
    ui->setupUi(this);
    m_room = new Room(m_flyCap, m_roomSize, this);
    m_roomRepr = new RoomGraphicsObject(m_room, m_cellPixelSize);
    GraphicsView *view = qobject_cast<GraphicsView*>(ui->graphicsView);
    if(!view)
    {
        assert(false);
        exit(-1);
    }
    ui->graphicsView->setScene(new QGraphicsScene());
    ui->graphicsView->scene()->addItem(m_roomRepr);
}

MainWindow::~MainWindow()
{

}

void MainWindow::fitView()
{
    ui->graphicsView->fitInView(m_roomRepr->boundingRect(), Qt::KeepAspectRatio);
}

QString MainWindow::formFlyStats()
{
    QString str;
    auto aliveFlies = *m_room->aliveFlies();
    auto deadFlies = *m_room->graveyard();
    str += "<html><head></head><body>";

    str += "<h1 style=\"color:#00aa00\";> Alive flies: </h1>";

    for(auto fly : aliveFlies)
    {
        double averVel = 0;
        if(fly->run() > 0)
        {
            averVel = (double)fly->run() / ((double)fly->lifeTime() / 1000.0); // msec to sec
        }
        str += QString("<p>") + "number " + QString::number(fly->id()) +
                "; run: " + QString::number(fly->run()) + "; average vel:" +
                QString::number(averVel) +"/sec </p>";
    }

    str += "<h1 style=\"color:#FF0000\";> DEAD flies: </h1>";

    for(auto fly : deadFlies)
    {
        double averVel = 0;
        if(fly->run() > 0)
        {
            averVel = (double)fly->run() / ((double)fly->lifeTime() / 1000.0);// msec to sec
        }
        str += QString("<p>") + "number " + QString::number(fly->id()) +
                "; run: " + QString::number(fly->run()) + "; average vel:" +
                QString::number(averVel) +"/sec </p>";
    }

    str += "</body></html>";
    return str;
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
    ui->draggableFly->setStupidity(arg1);
}

void MainWindow::on_playPauseBtn_toggled(bool checked)
{
    if(checked)
    {
        m_roomRepr->onStarted();
        m_room->start();
    }
    else
    {
        m_room->stop();
        QScrollArea *scrollArea = new QScrollArea;
        QWidget *statWgt = new QWidget(nullptr);
        statWgt->setAttribute(Qt::WA_DeleteOnClose);
        statWgt->setLayout(new QBoxLayout(QVBoxLayout::Direction::Down));
        QLabel *label = new QLabel;
        statWgt->layout()->addWidget(label);
        label->setText(formFlyStats());
        scrollArea->setWidget(statWgt);
        scrollArea->show();
        m_roomRepr->onStopped();
    }
}

void MainWindow::on_clearButton_clicked()
{
    m_room->clearFlies();
}
