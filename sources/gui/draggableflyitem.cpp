#include "draggableflyitem.h"
#include "common/common.h"
#include <QMimeData>
#include <QMouseEvent>
#include <QDrag>

DraggableFlyItem::DraggableFlyItem(QWidget *parent) : QLabel(parent)
{
    setFixedSize(64, 64);
    setPixmap(QPixmap(":/sources/resources/images/fly.png").scaled(64, 64, Qt::KeepAspectRatio, Qt::SmoothTransformation));
}

void DraggableFlyItem::mousePressEvent(QMouseEvent *ev)
{
    QByteArray itemData;
    itemData.append(common::uintToBArr(m_stupidity));
    QMimeData *mimeData = new QMimeData;
    mimeData->setData("flyData", itemData);
    QDrag drag(this);
    drag.setMimeData(mimeData);
    drag.setPixmap(*pixmap());
    drag.setHotSpot(ev->pos());
    drag.exec();
}

void DraggableFlyItem::setStupidity(int stupidity)
{
    m_stupidity = stupidity;
}

