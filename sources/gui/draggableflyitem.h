#ifndef DRAGGABLEFLYITEM_H
#define DRAGGABLEFLYITEM_H

#include <QLabel>

class DraggableFlyItem : public QLabel
{
    Q_OBJECT
public:
    explicit DraggableFlyItem(QWidget *parent = nullptr);

signals:

public slots:
    void mousePressEvent(QMouseEvent * ev) override;
    void setStupidity(int stupidity);

private:
    int m_stupidity = 1000;
};

#endif // DRAGGABLEFLYITEM_H
