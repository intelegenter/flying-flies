QT     += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    sources/common/common.cpp \
    sources/data/fly.cpp \
    sources/data/room.cpp \
    sources/data/roomcell.cpp \
    sources/graphics/flygraphicsobject.cpp \
    sources/graphics/graphicsview.cpp \
    sources/graphics/roomgraphicsobject.cpp \
    sources/gui/draggableflyitem.cpp \
    sources/gui/inputdialog.cpp \
    sources/main.cpp \
    sources/gui/mainwindow.cpp

HEADERS += \
    sources/common/common.h \
    sources/data/fly.h \
    sources/data/room.h \
    sources/data/roomcell.h \
    sources/graphics/flygraphicsobject.h \
    sources/graphics/graphicsview.h \
    sources/graphics/roomgraphicsobject.h \
    sources/gui/draggableflyitem.h \
    sources/gui/inputdialog.h \
    sources/gui/mainwindow.h

FORMS += \
    sources/gui/inputdialog.ui \
    sources/gui/mainwindow.ui

INCLUDEPATH += sources/

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc
